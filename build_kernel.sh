#!/bin/sh
export KERNELDIR=`readlink -f .`
export INITRAMFS_SOURCE=`readlink -f $KERNELDIR/../redpill_icx_initramfs`
export PARENT_DIR=`readlink -f ..`
export USE_SEC_FIPS_MODE=true
export CURRENT_DIR=`readlink -f $KERNELDIR/..`

if [ "${1}" != "" ];then
  export KERNELDIR=`readlink -f ${1}`
fi

INITRAMFS_TMP="/tmp/initramfs-source"

if [ ! -f $KERNELDIR/.config ];
then
  make RedPillICX_defconfig
fi

. $KERNELDIR/.config

export ARCH=arm
export CROSS_COMPILE=~/CodeSourcery/Sourcery_G++_Lite/bin/arm-none-eabi-

TAR_NAME=$CURRENT_DIR/`echo $CONFIG_LOCALVERSION|cut -c 2-`.tar
ZIP_NAME=$CURRENT_DIR/`echo $CONFIG_LOCALVERSION|cut -c 2-`_CWM.zip
echo $TAR_NAME
echo $ZIP_NAME

echo compiling modules...
cd $KERNELDIR/
nice -n 10 make -j4 modules || exit 1

#remove previous initramfs files
rm -rf $INITRAMFS_TMP
rm -rf $INITRAMFS_TMP.cpio
#copy initramfs files to tmp directory
cp -ax $INITRAMFS_SOURCE $INITRAMFS_TMP
#clear git repositories in initramfs
find $INITRAMFS_TMP -name .git -exec rm -rf {} \;
#remove empty directory placeholders
find $INITRAMFS_TMP -name EMPTY_DIRECTORY -exec rm -rf {} \;
#remove mercurial repository
#rm -rf $INITRAMFS_TMP/.hg
#copy modules into initramfs
echo stripping modules...
mkdir -p $INITRAMFS/lib/modules
find -name '*.ko' -exec cp -av {} $INITRAMFS_TMP/lib/modules/ \;
${CROSS_COMPILE}strip --strip-unneeded $INITRAMFS_TMP/lib/modules/*

nice -n 10 make -j3 zImage CONFIG_INITRAMFS_SOURCE="$INITRAMFS_TMP" || exit 1

#cp $KERNELDIR/arch/arm/boot/zImage zImage
$KERNELDIR/mkshbootimg.py $KERNELDIR/zImage $KERNELDIR/arch/arm/boot/zImage $KERNELDIR/payload.tar $KERNELDIR/recovery.tar.xz

echo Preparing CWM and ODIN files...
tar cf $TAR_NAME zImage && ls -lh $TAR_NAME
cd -
cd $KERNELDIR/../Releases/CWM-RELEASE
cp $KERNELDIR/zImage .
rm -f $ZIP_NAME
zip -r $ZIP_NAME *
cd ..
cp $TAR_NAME $CURRENT_DIR/Releases
cp $ZIP_NAME $CURRENT_DIR/Releases
rm -f $TAR_NAME
rm -f $ZIP_NAME
echo Building RedPill complete...
